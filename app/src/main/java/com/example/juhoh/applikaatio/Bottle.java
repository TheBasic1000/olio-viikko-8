package com.example.juhoh.applikaatio;

public class Bottle {

    private String bottleName;
    private String manufacturer;
    private double total_energy;
    private double size;
    private double price;

    public Bottle() {
        bottleName = "Pepsi Max";
        manufacturer = "Pepsi";
        total_energy = 0.3;
        size = 0.5;
        price = 1.80;
    }

    public Bottle(String name, String manuf, double totE, double siz, double pric) {
        bottleName = name;
        manufacturer = manuf;
        total_energy = totE;
        size = siz;
        price = pric;
    }

    public String getName() {
        return bottleName;
    }

    public void setName(String n) {
        bottleName = n;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public double getEnergy() {
        return total_energy;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double s) {
        size = s;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double p) {
        price = p;
    }
}
