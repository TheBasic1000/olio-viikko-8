package com.example.juhoh.applikaatio;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class MainActivity extends Activity implements AdapterView.OnItemSelectedListener {

    Context context = null;
    TextView description;
    TextView outputView;
    SeekBar cashSlider;
    TextView cashView;
    Spinner brandSpinner;
    Spinner sizeSpinner;
    BottleDispenser bd = BottleDispenser.getInstance();
    int min = 1, max = 9, current = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        description = (TextView) findViewById(R.id.description);
        outputView = (TextView) findViewById(R.id.outputView);
        seekbar();
        brandSpinner = (Spinner) findViewById(R.id.brandSpinner);
        sizeSpinner = (Spinner) findViewById(R.id.sizeSpinner);
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this, R.array.bottles, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.sizes, android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        brandSpinner.setAdapter(adapter1);
        sizeSpinner.setAdapter(adapter2);
        brandSpinner.setOnItemSelectedListener(this);
    }

    public void seekbar() {
        cashSlider = (SeekBar) findViewById(R.id.cashSlider);
        cashView = (TextView) findViewById(R.id.cashView);
        cashSlider.setMax(max);
        cashView.setText(current + "€");

        cashSlider.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        current = progress + min;
                        cashView.setText(current + "€");
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                }
        );
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void addMoney(View v) {
        bd.addMoney(current);
        outputView.setText("Klink! " + current + "€ kilahti koneeseen!");
        cashSlider.setProgress(0);
    }

    public void buyBottle(View v) {
        String name = brandSpinner.getSelectedItem().toString();
        String size = sizeSpinner.getSelectedItem().toString();
        outputView.setText(bd.buyBottle(name, size));
    }

    public void returnMoney(View v) {
        outputView.setText(bd.returnMoney());
    }

    public void writeReceipt(View v) {
        try {
            OutputStreamWriter osw = new OutputStreamWriter(context.openFileOutput("receipt.txt", Context.MODE_PRIVATE));
            String s = "";
            s = ("Kuitti:\n" + bd.getLatestName() + " " + bd.getLatestSize() + "l " + bd.getLatestPrice() + "€");
            osw.write(s);
            osw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
