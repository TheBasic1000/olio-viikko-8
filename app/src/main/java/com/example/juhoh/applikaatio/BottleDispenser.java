package com.example.juhoh.applikaatio;

import java.util.ArrayList;
import java.text.DecimalFormat;

public class BottleDispenser {

    private int bottles;
    private ArrayList<Bottle> bottle_arraylist = new ArrayList<Bottle>();
    private double money;
    private String latestName;
    private String latestSize;
    private String latestPrice;
    private static DecimalFormat df2 = new DecimalFormat("#0.00");

    private static BottleDispenser bd = new BottleDispenser();

    private BottleDispenser() {
        bottles = 6;
        money = 0;
        Bottle b1 = new Bottle();
        b1.setName("Pepsi Max");
        b1.setSize(0.5);
        b1.setPrice(1.8);
        bottle_arraylist.add(b1);
        Bottle b2 = new Bottle();
        b2.setName("Pepsi Max");
        b2.setSize(1.5);
        b2.setPrice(2.2);
        bottle_arraylist.add(b2);
        Bottle b3 = new Bottle();
        b3.setName("Coca-Cola Zero");
        b3.setSize(0.5);
        b3.setPrice(2.0);
        bottle_arraylist.add(b3);
        Bottle b4 = new Bottle();
        b4.setName("Coca-Cola Zero");
        b4.setSize(1.5);
        b4.setPrice(2.5);
        bottle_arraylist.add(b4);
        Bottle b5 = new Bottle();
        b5.setName("Fanta Zero");
        b5.setSize(0.5);
        b5.setPrice(1.95);
        bottle_arraylist.add(b5);
        Bottle b6 = new Bottle();
        b6.setName("Fanta Zero");
        b6.setSize(0.5);
        b6.setPrice(1.95);
        bottle_arraylist.add(b6);
    }

    public static BottleDispenser getInstance() {
        return bd;
    }

    public String getLatestName() {
        return latestName;
    }

    public String getLatestSize() {
        return latestSize;
    }

    public String getLatestPrice() {
        return latestPrice;
    }

    public void addMoney(int amount) {
        money += amount;
    }

    public String buyBottle(String name, String size) {
        String size1 = size.substring(0, size.length() - 1);
        double s = Double.parseDouble(size1);
        String msg = "Haluamaanne tuotetta ei ole.";
        double price = 0.0;
        int n = 0;
        int bottle_found = 0;

        for (int i=0;i<bottles;i++) {
            if (bottle_arraylist.get(i).getName().equals(name) && bottle_arraylist.get(i).getSize()==s) {
                price = bottle_arraylist.get(i).getPrice();
                n = i;
                bottle_found = 1;
                break;
            }
        }
        if (bottle_found==0) {
            return msg;
        }
        if (money<price) {
            msg = "Syötä rahaa ensin!";
            return msg;
        }
        else {
            latestName = bottle_arraylist.get(n).getName();
            latestSize = String.valueOf(bottle_arraylist.get(n).getSize());
            latestPrice = String.valueOf(bottle_arraylist.get(n).getPrice());
            bottles -= 1;
            money -= price;
            msg = ("KACHUNK! " + bottle_arraylist.get(n).getName() + " tipahti masiinasta!");
            bottle_arraylist.remove(n);
            return msg;
        }
    }

    public String returnMoney() {
        String msg = ("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + df2.format(money) + "€");
        money = 0;
        return msg;
    }
}
